set(dep_message "\nConfiguration of EZtrace:\n"
  "	General:\n"
  "		Install directory : ${CMAKE_INSTALL_PREFIX}\n"
  "		Compiler: C       : ${CMAKE_C_COMPILER} (${CMAKE_C_COMPILER_ID})\n"
  "		Compiler: Fortran : ${CMAKE_Fortran_COMPILER} (${CMAKE_Fortran_COMPILER_ID})\n"
  "		BFD Path	  : ${BFD_LIBRARY}\n"
  "\n"
  "		Operating system  : ${CMAKE_SYSTEM_NAME}\n"
  "		Opcodes Found	  : ")
if (disassembler_exist)
  set(dep_message "${dep_message}" "Yes\n"
  "			Opcodes path: ${LIBOPCODES_LIBRARIES}")

else()
  set(dep_message "${dep_message}" "No\n")
endif()


set(dep_message "${dep_message}\n	Enabled modules:\n"
  "		MPI module    : ${EZTRACE_ENABLE_MPI}\n"
  "		CUDA module   : ${EZTRACE_ENABLE_CUDA}\n"
  "		STARPU module : ${EZTRACE_ENABLE_STARPU}")
set(dep_message "${dep_message}\n			StarPU library path :")
if (STARPU_FOUND)
  set(dep_message "${dep_message}" " ${STARPU_LIBRARY_DIRS}\n")
else()
  set(dep_message "${dep_message}" " StarPU not found\n")
endif()
set(dep_message "${dep_message}		OPENMP module : ${EZTRACE_ENABLE_OPENMP}\n"
    "		PAPI module   : ${EZTRACE_ENABLE_PAPI}\n")

message(${dep_message})