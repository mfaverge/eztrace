/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 *
 *
 * isize.c
 *
 * Wrapper to include the good library for determining the instruction size
 *
 *  Created on: 2 juil. 2011
 *      Author: Damien Martin-Guillerez <damien.martin-guillerez@inria.fr>
 */

#include <eztrace_config.h>
#include <binary.h>

#ifdef ENABLE_BINARY_INSTRUMENTATION

#ifdef __x86_64__
// x86_64
#ifndef __PPTRACE_ISIZE_TYPE
#include "arch/x86_64/trace.c"
#else

#if __PPTRACE_ISIZE_TYPE == PPTRACE_ISIZE_TYPE_OPCODE
#include "arch/x86_64/opcode.c"
#else // if ! (__PPTRACE_ISIZE_TYPE == PPTRACE_ISIZE_TYPE_OPCODE)
#include "arch/x86_64/trace.c"
#endif // ! (__PPTRACE_ISIZE_TYPE == PPTRACE_ISIZE_TYPE_OPCODE)
#endif // !defined(__PPTRACE_ISIZE_TYPE)
#elif HAVE_ARM
// ARMv7

#ifdef HAVE_LIBOPCODE
#include "arch/armv7/opcode.c"
#else
#warning Libopcode not available. pptrace will not work!
#endif

#endif
#endif
