/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 *
 *
 * trace.c
 *
 * Determining the size to override (by computing the size of instructions) using single step tracing
 */
#include <opcodes.h>
#include <tracing.h>
#include <errors.h>
#include <sys/types.h>
#include <sys/wait.h>

#define ARM_INSTR_SIZE_BYTE (ARM_INSTR_SIZE/8)
/*
 * This function determines the size of instructions that will be replaced.
 * Output is the size of the replacement
 */
ssize_t get_overridden_size(void *bin, pid_t child, word_uint symbol,
                            size_t trampoline_size) {

  /* On ARM mode, the size of instructions is static (32 bits). Thus, the size of instructions to replace
   * can be computed as follows.
   * Please note that this is only possible for ARM mode, *the result for Thumb mode is incorrect* !
   */
  return (size_t)(
      ((trampoline_size + symbol) / ARM_INSTR_SIZE_BYTE
        + ((trampoline_size + symbol) % ARM_INSTR_SIZE_BYTE == 0 ? 0 : 1))
        * ARM_INSTR_SIZE_BYTE - symbol);
}
