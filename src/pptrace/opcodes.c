/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 *
 *
 * opcodes.c
 *
 * Wrapper to include the good set of opcodes. If we ever
 * support multiple instruction set, we should certainly change that by an array
 * of opcodes.
 *
 *  Created on: 2 juil. 2011
 *      Author: Damien Martin-Guillerez <damien.martin-guillerez@inria.fr>
 */

#include <eztrace_config.h>
#include <opcodes.h>

#if __x86_64__
// x86_64

//#ifndef __PPTRACE_ARCH_TYPE
#include "cpu/intel.c"
//#else

//#if __PPTRACE_ARCH_TYPE == PPTRACE_ARCH_TYPE_XXX
//#include "opcodes/x x x.c"
//#else // if ! (__PPTRACE_ARCH_TYPE == PPTRACE_ARCH_TYPE_XXX)
//#include "opcodes/intel.c"
//#endif // ! (__PPTRACE_ARCH_TYPE == PPTRACE_ARCH_TYPE_XXX)

//#endif // !defined(__PTRACE_ARCH_TYPE)

#elif __arm__
// ARMv7 -- ARM cortex-A9
#include "cpu/arm.c"
#endif
