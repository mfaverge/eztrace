#include <string.h>
#include <stdlib.h>
#if HAVE_DEMANGLE
#include <demangle.h>

const char* ezt_demangle(const char* mangled_str) {
  /* todo: add an option to get only the function name */
  /* get the full function prototype (including parameters and return type)  */

  char* res_str = cplus_demangle(mangled_str, DMGL_AUTO | DMGL_PARAMS);

  if(!res_str) {
    /* demangling failed. return the mangled string */
    int len = strlen(mangled_str) + 1;
    res_str = malloc(sizeof(char*) * len);
    strncpy(res_str, mangled_str, len);
  }
  return res_str;
}

int ezt_demangle_init() {
  return 1;
}

#else  /* HAVE_DEMANGLE */

/* Libiberty is not available, return a copy of the mangled string */
const char* ezt_demangle(const char* mangled_str) {
  int len = strlen(mangled_str) + 1;
  char* res_str = malloc(sizeof(char*) * len);
  strncpy(res_str, mangled_str, len);
  return res_str;
}

int ezt_demangle_init() {
  return 0;
}

#endif	/* HAVE_DEMANGLE */
