/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

static void MPI_Alltoall_prolog (CONST void *sendbuf __attribute__((unused)),
				 int sendcount __attribute__((unused)),
				 MPI_Datatype sendtype,
				 void *recvbuf __attribute__((unused)),
				 int recvcnt __attribute__((unused)),
				 MPI_Datatype recvtype,
				 MPI_Comm comm)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int ssize=0;
  int rsize =0;
  if(sendtype != MPI_DATATYPE_NULL)
    MPI_Type_size(sendtype, &ssize);
  if(recvtype != MPI_DATATYPE_NULL)
    MPI_Type_size(recvtype, &rsize);

  int data_size = sendcount * ssize;

  EZTRACE_EVENT_PACKED_4(EZTRACE_MPI_START_Alltoall, (app_ptr)comm, size, rank, data_size);
}

static int MPI_Alltoall_core(CONST void *sendbuf, int sendcount,
                             MPI_Datatype sendtype, void *recvbuf, int recvcnt,
                             MPI_Datatype recvtype, MPI_Comm comm)
{
  return libMPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcnt,
                         recvtype, comm);
}

static void MPI_Alltoall_epilog (CONST void *sendbuf __attribute__((unused)),
				 int sendcount __attribute__((unused)),
				 MPI_Datatype sendtype,
				 void *recvbuf __attribute__((unused)),
				 int recvcnt __attribute__((unused)),
				 MPI_Datatype recvtype,
				 MPI_Comm comm)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int ssize=0;
  int rsize =0;
  if(sendtype != MPI_DATATYPE_NULL)
    MPI_Type_size(sendtype, &ssize);
  if(recvtype != MPI_DATATYPE_NULL)
    MPI_Type_size(recvtype, &rsize);

  EZTRACE_EVENT_PACKED_3(EZTRACE_MPI_STOP_Alltoall, (app_ptr)comm, size, rank);
}

int MPI_Alltoall(CONST void *sendbuf, int sendcount, MPI_Datatype sendtype,
                 void *recvbuf, int recvcnt, MPI_Datatype recvtype,
                 MPI_Comm comm)
{
  FUNCTION_ENTRY;
   MPI_Alltoall_prolog(sendbuf, sendcount, sendtype, recvbuf, recvcnt, recvtype,
		                        comm);
  int ret = MPI_Alltoall_core(sendbuf, sendcount, sendtype, recvbuf, recvcnt,
                              recvtype, comm);
    MPI_Alltoall_epilog(sendbuf, sendcount, sendtype, recvbuf, recvcnt, recvtype,
                     comm);

  return ret;
}

void mpif_alltoall_(void *sbuf, int *scount, MPI_Fint *sd, void *rbuf,
                    int *rcount, MPI_Fint *rd, MPI_Fint *c, int *error) {
  FUNCTION_ENTRY;
  MPI_Datatype c_stype = MPI_Type_f2c(*sd);
  MPI_Datatype c_rtype = MPI_Type_f2c(*rd);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  void *c_sbuf = CHECK_MPI_IN_PLACE(sbuf);
  void *c_rbuf = CHECK_MPI_IN_PLACE(rbuf);

  MPI_Alltoall_prolog(c_sbuf, *scount, c_stype, c_rbuf, *rcount, c_rtype, c_comm);
  *error = MPI_Alltoall_core(c_sbuf, *scount, c_stype, c_rbuf, *rcount, c_rtype,
                             c_comm);
  MPI_Alltoall_epilog(c_sbuf, *scount, c_stype, c_rbuf, *rcount, c_rtype, c_comm);
}
