/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright © CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

#ifdef USE_MPI3
static void MPI_Iallgather_prolog(CONST void __attribute__((unused)) *sendbuf,
				  int sendcount,
				  MPI_Datatype sendtype,
				  void __attribute__((unused)) *recvbuf,
				  int __attribute__((unused)) recvcount,
				  MPI_Datatype recvtype,
				  MPI_Comm comm,
				  MPI_Request *r) {
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int dsize;
  /* retrieve the size of the datatype so that we can compute the message length */
  MPI_Type_size(sendtype, &dsize);
  int data_size = sendcount * dsize;
  EZTRACE_EVENT_PACKED_5(EZTRACE_MPI_IALLGATHER, (app_ptr)comm, size, rank, (app_ptr)r, data_size);
}


static int MPI_Iallgather_core(CONST void *sendbuf, int sendcount, MPI_Datatype sendtype,
			       void *recvbuf, int recvcount, MPI_Datatype recvtype,
			       MPI_Comm comm, MPI_Request *r)
{
  return libMPI_Iallgather(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, r);
}

static void MPI_Iallgather_epilog(CONST void __attribute__((unused)) *sendbuf,
				  int sendcount  __attribute__((unused)),
				  MPI_Datatype sendtype __attribute__((unused)),
				  void *recvbuf __attribute__((unused)),
				  int recvcount __attribute__((unused)),
				  MPI_Datatype recvtype __attribute__((unused)),
				  MPI_Comm comm __attribute__((unused)),
				  MPI_Request *r) {
  EZTRACE_EVENT_PACKED_1(EZTRACE_MPI_STOP_IALLGATHER, (app_ptr)r);
}

int MPI_Iallgather(CONST void *sendbuf, int sendcount, MPI_Datatype sendtype,
		   void *recvbuf, int recvcount, MPI_Datatype recvtype,
		   MPI_Comm comm, MPI_Request *r)
{
  FUNCTION_ENTRY;
  MPI_Iallgather_prolog(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, r);
  int ret = MPI_Iallgather_core(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, r);
  MPI_Iallgather_epilog(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, r);
  return ret;
}

void mpif_iallgather_(void *sbuf, int *scount, MPI_Fint *sd,
		      void *rbuf, int *rcount, MPI_Fint *rd,
		      MPI_Fint *c, MPI_Fint *r,int *error)
{
  FUNCTION_ENTRY;
  MPI_Datatype c_stype = MPI_Type_f2c(*sd);
  MPI_Datatype c_rtype = MPI_Type_f2c(*rd);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Request c_req = MPI_Request_f2c(*r);

  MPI_Iallgather_prolog(sbuf, *scount, c_stype, rbuf, *rcount, c_rtype, c_comm, r);
  *error = MPI_Iallgather_core(sbuf, *scount, c_stype, rbuf, *rcount, c_rtype, c_comm, &c_req);
  *r= MPI_Request_c2f(c_req);
   MPI_Iallgather_epilog(sbuf, *scount, c_stype, rbuf, *rcount, c_rtype, c_comm, r);
}
#endif
