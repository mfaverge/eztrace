/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

static void MPI_Cancel_prolog(MPI_Fint *req) {
  EZTRACE_EVENT_PACKED_1(EZTRACE_MPI_CANCEL, (app_ptr)req);
}

static int MPI_Cancel_core(MPI_Request *request) {
  return libMPI_Cancel(request);
}


int MPI_Cancel(MPI_Request *req) {
  FUNCTION_ENTRY;
  MPI_Cancel_prolog((MPI_Fint*) req);
  int ret = MPI_Cancel_core(req);
  return ret;
}

void mpif_cancel_(MPI_Fint *r, int *error) {
  FUNCTION_ENTRY;
  MPI_Request c_req = MPI_Request_f2c(*r);
  MPI_Cancel_prolog(r);
  *error = MPI_Cancel_core(&c_req);
}
